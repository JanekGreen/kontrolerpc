package klient;

import java.awt.AWTException;
import java.awt.EventQueue;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class okno {

	private JFrame frame;
	private JTextField ipSerwer;
	private JTextField nrPort;
	private Socket gniazdo;
	private odbieracz odb;
	public static double stopienkompresji = 0.7;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
					okno window = new okno();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public okno() {
		initialize();
		try {
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 346, 216);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPodajAdresIp = new JLabel("Podaj IP serwera:");
		lblPodajAdresIp.setBounds(10, 47, 134, 14);
		frame.getContentPane().add(lblPodajAdresIp);
		
		ipSerwer = new JTextField();
		ipSerwer.setBounds(128, 44, 134, 20);
		frame.getContentPane().add(ipSerwer);
		ipSerwer.setColumns(10);
		
		nrPort = new JTextField();
		nrPort.setBounds(128, 72, 53, 20);
		frame.getContentPane().add(nrPort);
		nrPort.setColumns(10);
		
		JLabel lblNumerPortu = new JLabel("Numer portu:");
		lblNumerPortu.setBounds(10, 72, 71, 14);
		frame.getContentPane().add(lblNumerPortu);
		
		JButton btnPocz = new JButton("Po\u0142\u0105cz");
		btnPocz.setBounds(115, 125, 89, 23);
		frame.getContentPane().add(btnPocz);
		
		JButton btnNewButton = new JButton("Roz\u0142\u0105cz");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
try {
	gniazdo.close();
} catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
gniazdo = null;
			}
		});
		btnNewButton.setBounds(214, 125, 89, 23);
		frame.getContentPane().add(btnNewButton);
		btnPocz.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				komunikacja();
			}
		});
	}
	
	public void komunikacja()
	{
	
		
		try {
			
			gniazdo = new Socket(ipSerwer.getText(), Integer.valueOf(nrPort.getText()));
			
		
		
			wysylacz wys = new  wysylacz(gniazdo);
			wys.start();
			odb = new  odbieracz(gniazdo);
			odb.start();
			
		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	
		}
	
	}
}
