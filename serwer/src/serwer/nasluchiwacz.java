package serwer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;

public class nasluchiwacz implements Runnable{

	private ServerSocket serwer;
	public static JList<String> lista; 
	private ArrayList<Soket> gniazda;
	public static DefaultListModel<String> komputery = new DefaultListModel<String>();
	
	public nasluchiwacz(ServerSocket serwer, JList lista, ArrayList<Soket> gniazda)
	{
		
		this.serwer = serwer;
	
		this.lista = lista;
		this.gniazda = gniazda;
		this.komputery = komputery;
	}
	
	SocketChannel kanal;

	@Override
	public void run() {	
		
		lista.setModel(komputery);
		
	while(!serwer.isClosed()) {
		System.out.println("oczekuje... !");
		try {
			System.out.println(serwer.getInetAddress().getLocalHost().getHostAddress().toString());
		} catch (UnknownHostException e1) {
		}
		try {
			Socket gniazdo = serwer.accept();
	
			gniazda.add(new Soket(gniazdo));
		} catch (IOException e) {
			break;
		}
		
		komputery.addElement("Komputer " +" (" + gniazda.get(gniazda.size()-1).gniazdo.getRemoteSocketAddress().toString().substring(1) + ")");
		System.out.println("poloczony: " + gniazda.get(gniazda.size()-1).gniazdo.getRemoteSocketAddress().toString());
		lista.repaint();
		new Thread(new sprawdzacz(gniazda, komputery, serwer)).start();

		}
	komputery.clear();
	gniazda.clear();
	System.out.println("koniec");
	}

}
