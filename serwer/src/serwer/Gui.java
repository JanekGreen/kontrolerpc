package serwer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;






public class Gui extends JFrame {

	public JPanel okno;
	private Przechwytywacz przechwytywacz;

	/**
	 * Create the frame.
	 * @param socket 
	 */
	public static odbieracz odb;
	public Gui(int width, int height, Soket gniazdo ) {
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setPreferredSize(new Dimension(width, height)); // bajer, konstruktor z rozdzielczoscia okna
		pack();
		
		
	    addWindowListener(new WindowAdapter() {
			@Override
	        public void windowClosing(WindowEvent event) {
	        	svr.dalej = false;
	      
	        	svr.aktywny = -1;
	        	sprawdzacz.wyslij = null;
	        	dispose();	        }
	    });

		okno = new JPanel();
		okno.setLayout(new BorderLayout(0, 0));
		setContentPane(okno);
		setVisible(true);
		okno.setVisible(true);
		okno.setFocusable(true);  //fest wazne key listener na tym polega
		odb = new odbieracz(gniazdo, this);
		odb.start();



	}
	
	 public void rysuj(ImageIcon obraz)
	{
		
		Image obr = obraz.getImage();
 		Graphics2D g = (Graphics2D) okno.getGraphics();
		g.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING,
            RenderingHints.VALUE_RENDER_QUALITY));
 	

    g.drawImage(obr,0,0,okno.getWidth(),okno.getHeight(), null);
	
	
	}
	 
	 public void blad(String napis)
		{
			
	 	Graphics2D g = (Graphics2D) okno.getGraphics();
	 
		g.addRenderingHints(new RenderingHints(
	 	        RenderingHints.KEY_TEXT_ANTIALIASING,
	 	        RenderingHints.VALUE_TEXT_ANTIALIAS_ON));
	 	 
	    g.clearRect(0, 0, okno.getWidth(), okno.getHeight());
	    
	    g.setFont(new Font("Calibri", Font.PLAIN, 35));
		g.drawString(napis, okno.getWidth()/3, okno.getHeight()/2);
		
		}

}
