package serwer;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JList;

import java.awt.Font;

import javax.swing.ListSelectionModel;
import javax.swing.AbstractListModel;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamField;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.RepaintManager;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSlider;
import javax.swing.JCheckBox;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class svr {

	private JFrame frame;
	
	private JLabel lblAdresIp;
	private ArrayList<Soket> gniazda = new ArrayList<Soket>();
	private ServerSocket serwer;
	
	private JTextField textField;
	private JLabel lblNewLabel;
	private JButton btnStart;
	static public JList list;
	
	private Boolean spr = true;
	public static int aktywny = -1; 
	public static Boolean dalej;
	
	
	public static double stopienkompresji = 0.7;
	
	private Thread nasluch;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					svr window = new svr();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	Socket gniazdo;

	/**
	 * Create the application.
	 */
	public svr() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 375, 474);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		list = new JList();		
		list.setValueIsAdjusting(true);
		list.setBounds(27, 30, 309, 233);
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setFont(new Font("Calibri", Font.PLAIN, 17));
		panel.add(list);
		
		JButton btnPocz = new JButton("Po\u0142\u0105cz");
		btnPocz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				połącz();
			}
		});
		btnPocz.setBounds(27, 274, 89, 23);
		panel.add(btnPocz);
		
		lblAdresIp = new JLabel("Adres IP");
		lblAdresIp.setBounds(27, 344, 146, 23);
		panel.add(lblAdresIp);
		
	
		
			textField = new JTextField();
			textField.setBounds(60, 378, 74, 23);
			panel.add(textField);
			textField.setColumns(10);
			
			lblNewLabel = new JLabel("Port: ");
			lblNewLabel.setBounds(27, 378, 36, 23);
			panel.add(lblNewLabel);
			
			btnStart = new JButton("Start");
		
			try {
				lblAdresIp.setText("Adres IP: " + InetAddress.getLocalHost().getHostAddress().toString()); //wy�wietlenie adresu IP
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			btnStart.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					
					if(spr) {
						
					try {
						serwer = new ServerSocket(Integer.valueOf(textField.getText()));
					} catch (NumberFormatException | IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					nasluchiwacz nas = new nasluchiwacz(serwer, list, gniazda);
					nasluch = new Thread(nas);
					nasluch.start();
					}
					
				}
			});
			btnStart.setBounds(144, 378, 89, 23);
			panel.add(btnStart);
			JSlider slider = new JSlider();
			slider.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					stopienkompresji = slider.getValue()/100.0;
				}
			});
			slider.setValue(70);
			slider.setBounds(157, 303, 133, 30);
			panel.add(slider);
			
			JLabel lblJako = new JLabel("Stopień kompresji:");
			lblJako.setBounds(159, 278, 112, 14);
			panel.add(lblJako);
			
			JCheckBox chckbxNewCheckBox = new JCheckBox("PNG");
			chckbxNewCheckBox.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent arg0) {
					if(chckbxNewCheckBox.isSelected()) { slider.setEnabled(false); stopienkompresji = -1;} else {slider.setEnabled(true); stopienkompresji = slider.getValue()/100.0;} ;
				}
			});
			chckbxNewCheckBox.setBounds(296, 303, 57, 23);
			panel.add(chckbxNewCheckBox);
		
	}
	static public Gui guji = null;
	static public ObjectInputStream strumień;
	static public ObjectOutputStream strumień2;						
	private void połącz()
	{
		System.out.println(gniazda.size());
		
		if(list.getSelectedIndex() != -1)
		guji = new Gui(1024, 600, gniazda.get(list.getSelectedIndex()));
		aktywny = list.getSelectedIndex();
		dalej = true;
		}
}
