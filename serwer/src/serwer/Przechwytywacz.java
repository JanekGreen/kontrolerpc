package serwer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.net.Socket;

import javax.swing.JPanel;

public class Przechwytywacz implements MouseListener, MouseMotionListener, KeyListener,MouseWheelListener  {

	private JPanel okno;
	private Dimension rozdzielczoscKlienta;
	private wysylacz wyslij;
	 

	
		
	public Przechwytywacz(JPanel okno, int width, int height, Soket gniazdo)
	{
		this.okno = okno; 
		rozdzielczoscKlienta = new Dimension(width, height);
	    this.okno.addMouseListener(this);
		this.okno.addKeyListener(this);
		this.okno.addMouseMotionListener(this);
		this.okno.addMouseWheelListener(this);
		wyslij = new wysylacz(gniazdo);
		wyslij.zmianajakosci(svr.stopienkompresji);
		sprawdzacz.ustawwysylacza(wyslij);
	}
	
	
	@Override
	public void keyPressed(KeyEvent e) {
		
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
		wyslij.wyslij(KeyEvent.getExtendedKeyCodeForChar(e.getKeyChar()));
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {

		 
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
		 if(e.getButton() == MouseEvent.BUTTON1){ // dla lewego perzycisku myszy
			int x= e.getX();
			int y= e.getY();
			double mnoznikX = rozdzielczoscKlienta.getWidth()/okno.getWidth();
			double mnoznikY = rozdzielczoscKlienta.getHeight()/okno.getHeight();
		 	wyslij.wyslijdrag(x*= mnoznikX, y*= mnoznikY);
		 }
		 else 
		 {
			 
			wyslij.wyslijprzyciskmyszki(InputEvent.BUTTON3_DOWN_MASK);
			 
		 }
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
		 if(e.getButton() == MouseEvent.BUTTON1){ // dla lewego perzycisku myszy
		int x= e.getX();
		int y= e.getY();
		double mnoznikX = rozdzielczoscKlienta.getWidth()/okno.getWidth();
		double mnoznikY = rozdzielczoscKlienta.getHeight()/okno.getHeight();
		 wyslij.wyslijdrag(x*= mnoznikX, y*= mnoznikY);
		 
		 }
		   
		
	}


	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
		
	}


	@Override
	public void mouseMoved(MouseEvent e) {
		int x= e.getX();
		int y= e.getY();
		double mnoznikX = rozdzielczoscKlienta.getWidth()/okno.getWidth();
		double mnoznikY = rozdzielczoscKlienta.getHeight()/okno.getHeight();
	    
	 wyslij.wyslij(x*= mnoznikX, y*= mnoznikY);
		   
		
	}


	@Override
	public void mouseWheelMoved(MouseWheelEvent arg0) {
		
		int ile = arg0.getWheelRotation();
		 wyslij.wyslijscroll(ile);
		
	}

}
