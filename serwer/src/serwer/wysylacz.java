package serwer;

import java.awt.Dimension;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class wysylacz {

	Soket gniazdo;
    private final Object lock = new Object();

	
	public wysylacz(Soket gniazdo)

	{
		this.gniazdo = gniazdo;
	}
	
	public void zmianajakosci(double stopien)
	{
		synchronized (lock) {
			try {
			gniazdo.strumienwyjsciowy.write(3);
			gniazdo.strumienwyjsciowy.flush(); // nie wiem czy potrzebne 

			gniazdo.strumienwyjsciowy.writeDouble(stopien);
			gniazdo.strumienwyjsciowy.reset();
			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void wyslij(int klawisz)
	{
		
		try {
			
		synchronized (lock) {
			gniazdo.strumienwyjsciowy.write(0);
			gniazdo.strumienwyjsciowy.flush(); // nie wiem czy potrzebne 

			gniazdo.strumienwyjsciowy.writeInt(klawisz);
			gniazdo.strumienwyjsciowy.reset();
		}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	
	public void wyslij(int X, int Y)
	{
		
		try {
			
		synchronized (lock) {
			gniazdo.strumienwyjsciowy.write(1);
			gniazdo.strumienwyjsciowy.flush();// nie wiem czy potrzebne, bez tez chodzi

			gniazdo.strumienwyjsciowy.writeObject(new Dimension(X, Y));	
			gniazdo.strumienwyjsciowy.reset();

		}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	
	public void wyslijprzyciskmyszki(int przycisk)
	{
		
		try {
			
		synchronized (lock) {
			gniazdo.strumienwyjsciowy.write(2);
			gniazdo.strumienwyjsciowy.writeInt(przycisk);	
			gniazdo.strumienwyjsciowy.reset();

		}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

	public void wyslijscroll(int _ile)
	{
		
		try {
			
		synchronized (lock) {
			gniazdo.strumienwyjsciowy.write(4);
			gniazdo.strumienwyjsciowy.writeInt(_ile);	
			gniazdo.strumienwyjsciowy.reset();

		}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	public void wyslijdrag(int X, int Y)
	{
		
		try {
			
		synchronized (lock) {
			gniazdo.strumienwyjsciowy.write(5);
			gniazdo.strumienwyjsciowy.flush();// nie wiem czy potrzebne, bez tez chodzi

			gniazdo.strumienwyjsciowy.writeObject(new Dimension(X, Y));	
			gniazdo.strumienwyjsciowy.reset();

		}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	
}
